# Start the application 
1. Create a virtualenv 
    ```bash 
        virtualenv .venv
    ```
2. Source it 
    ```bash 
        source .venv/bin/activate
    ```
3. Install dependencies
    ```bash 
        pip3 install -r requirements.txt
    ```
3. Run Main App
    ```bash 
        python3 main.py
    ```
3. Check the application : 
    http://localhost:8000/docs#/

