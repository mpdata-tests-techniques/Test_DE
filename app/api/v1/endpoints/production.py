import os

import requests
from dotenv import load_dotenv
from fastapi import APIRouter

from app.db.crud import (
    insert_consumption,
    insert_forecast_consumption,
    insert_production_value,
)

router = APIRouter()
load_dotenv()

BASE_URL = os.getenv("BASE_URL")

# TODO: 3.3.A Implement the function to get all the production, consumptions, forecast values
# from the database with 3 several fastapi routes


@router.post("/actual_generations_per_production_type")
def post_actual_generations_per_production_type(bearer: str):
    headers = {"Authorization": f"Bearer {bearer}"}
    response = requests.get(
        f"{BASE_URL}/open_api/actual_generation/v1/actual_generations_per_production_type",
        headers=headers,
    )
    if response.status_code == 200:
        actual_generations = response.json()

        for actual_generation in actual_generations[
            "actual_generations_per_production_type"
        ]:
            for value in actual_generation["values"]:
                try:
                    insert_production_value(
                        value["start_date"],
                        value["end_date"],
                        value.get("updated_date"),
                        value["value"],
                        actual_generation["production_type"],
                    )
                except Exception as e:
                    print(value)
                    return {"message": f"Error storing data: {e}"}

        return {"message": "Data stored successfully"}
    else:
        return {"message": "Error storing data"}


@router.post("/consolidated_power_consumption")
def post_consolidated_power_consumption(bearer: str):
    headers = {"Authorization": f"Bearer {bearer}"}
    response = requests.get(
        f"{BASE_URL}/open_api/consolidated_consumption/v1/consolidated_power_consumption",
        headers=headers,
    )
    if response.status_code == 200:
        consumption_datas = response.json()
        for consumption_data in consumption_datas["consolidated_power_consumption"]:
            for value in consumption_data["values"]:
                try:
                    insert_consumption(
                        value["start_date"],
                        value["end_date"],
                        value["value"],
                        value.get("updated_date"),
                        value.get("status"),
                    )
                except Exception as e:
                    print(value)
                    return {"message": f"Error storing data: {e}"}
        return {"message": "Data stored successfully"}
    else:
        return {"message": "Error storing data"}


@router.post("/weekly_forecasts")
def post_weekly_forecasts(bearer: str):
    headers = {"Authorization": f"Bearer {bearer}"}
    response = requests.get(
        f"{BASE_URL}/open_api/consumption/v1/weekly_forecasts", headers=headers
    )
    if response.status_code == 200:
        weekly_forecasts = response.json()
        for weekly_forecast in weekly_forecasts["weekly_forecasts"]:
            peak = [
                weekly_forecast["peak"]["peak_hour"],
                weekly_forecast["peak"]["value"],
                weekly_forecast["peak"]["temperature"],
                weekly_forecast["peak"]["temperature_deviation"],
            ]
            for value in weekly_forecast["values"]:
                try:
                    insert_forecast_consumption(
                        value["start_date"],
                        value["end_date"],
                        value["value"],
                        value.get("updated_date"),
                        peak[0],
                        peak[1],
                        peak[2],
                        peak[3],
                    )
                except Exception as e:
                    print(value)
                    return {"message": f"Error storing data: {e}"}

    return {"message": "Data stored successfully"}
