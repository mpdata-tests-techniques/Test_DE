import sqlite3


# Function to create the tables
def create_tables():
    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    # Create productions table
    c.execute(
        """CREATE TABLE IF NOT EXISTS productions (
                    name VARCHAR(200) PRIMARY KEY
                )"""
    )

    # Create production_values table
    c.execute(
        """CREATE TABLE IF NOT EXISTS production_values (
                    id INTEGER PRIMARY KEY,
                    start_date TIMESTAMP NOT NULL,
                    end_date TIMESTAMP NOT NULL,
                    updated_date TIMESTAMP,
                    value INTEGER NOT NULL,
                    production VARCHAR(200),
                    FOREIGN KEY (production) REFERENCES productions(name),
                    UNIQUE (start_date, updated_date, production)
                )"""
    )

    # Create consumptions table
    c.execute(
        """CREATE TABLE IF NOT EXISTS consumptions (
                    id INTEGER PRIMARY KEY,
                    start_date TIMESTAMP NOT NULL,
                    end_date TIMESTAMP NOT NULL,
                    value INTEGER NOT NULL,
                    updated_date TIMESTAMP,
                    status VARCHAR(200) NOT NULL,
                    UNIQUE (start_date, updated_date, status)
                )"""
    )

    # Create forecast_consumption table
    c.execute(
        """CREATE TABLE IF NOT EXISTS forecast_consumption (
                    id INTEGER PRIMARY KEY,
                    start_date TIMESTAMP NOT NULL,
                    end_date TIMESTAMP NOT NULL,
                    value INTEGER NOT NULL,
                    updated_date TIMESTAMP,
                    peak_hour TIMESTAMP,
                    peak_value INTEGER,
                    peak_temperature FLOAT,
                    peak_temperature_deviation FLOAT,
                    UNIQUE (start_date, updated_date)
                )"""
    )

    conn.commit()
    conn.close()


# Function to insert data into productions table
def insert_production(name):
    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    c.execute("INSERT INTO productions (name) VALUES (?)", (name,))

    conn.commit()
    conn.close()


# Function to insert data into production_values table
def insert_production_value(start_date, end_date, updated_date, value, production):
    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    c.execute(
        "INSERT or IGNORE INTO production_values (start_date, end_date, updated_date, value, production) VALUES (?, ?, ?, ?, ?)",
        (start_date, end_date, updated_date, value, production),
    )

    # print("Production value added")
    conn.commit()
    conn.close()


# Function to insert data into consumptions table
def insert_consumption(start_date, end_date, value, updated_date, status):
    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    c.execute(
        "INSERT or IGNORE INTO consumptions (start_date, end_date, value, updated_date, status) VALUES (?, ?, ?, ?, ?)",
        (start_date, end_date, value, updated_date, status),
    )

    conn.commit()
    conn.close()


# Function to insert data into forecast_consumption table
def insert_forecast_consumption(
    start_date,
    end_date,
    value,
    updated_date,
    peak_hour,
    peak_value,
    peak_temperature,
    peak_temperature_deviation,
):
    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    c.execute(
        "INSERT or IGNORE INTO forecast_consumption (start_date, end_date, value, updated_date, peak_hour, peak_value, peak_temperature, peak_temperature_deviation) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
        (
            start_date,
            end_date,
            value,
            updated_date,
            peak_hour,
            peak_value,
            peak_temperature,
            peak_temperature_deviation,
        ),
    )

    print("Forecast consumption added")
    conn.commit()
    conn.close()


# TODO: 3.3.A - Implement the function to get all the production, consumptions, forecast values from the database
