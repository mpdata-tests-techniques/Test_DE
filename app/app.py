import os

import requests
from dotenv import load_dotenv
from fastapi import FastAPI

from .api.v1.endpoints.production import router as ProdcutionRouter

app = FastAPI()

load_dotenv()

API_KEY = os.getenv("API_KEY")
BASE_URL = os.getenv("BASE_URL")

app.include_router(ProdcutionRouter, tags=["Production"], prefix="/production")


@app.post("/authent", tags=["Authentification"])
async def authentification():
    authent = f"{BASE_URL}/token/oauth"
    headers = {
        "Authorization": f"Basic {API_KEY}",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    try:
        r = requests.post(authent, headers=headers)
        r.raise_for_status()
        response = r.json()
        bearer = response["access_token"]
        return bearer
    except requests.exceptions.RequestException as e:
        # Handle request exception
        print(f"Request Exception: {e}")
        return None
    except KeyError as e:
        # Handle missing key in response
        print(f"KeyError: {e}")
        return None


@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Home "}
